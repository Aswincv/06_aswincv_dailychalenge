/*
Name of the project : Dailychallenge
File name : display.js
Description : reverse linkedList
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [5,2,6,3,7,8,2,9,2]
Output : 2,9,2,8,7,3,6,2,5
*/


class Node {
    constructor(element) {
        this.element = element;
        this.next = null;
    }
}
var reversList = [];
class linkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    addElement(num) {
        if(this.head == null) {
            var node = new Node(num);
            this.head = node;
            this.size ++;
        }else {
            var prev ;
            var current = this.head;
            while(current) {
                prev = current;
                current = current.next;
            }
            var node = new Node(num);
            prev.next = node;
            this.size ++;
        }
    }
    show() {
      var curr = this.head;
      var str = "";
      while (curr) {
         str += curr.element + " ";
         curr = curr.next;
      }
      console.log(str);
    }
    rvrs(root) {
             if(root.next == null) {
                 reversList.push(root.element);
                 return;
             }else {
                 this.rvrs(root.next);
                 reversList.push(root.element);
                 return;
             }
    }
    reversing() {
        var curr = this.head;
        if(curr) {
            this.rvrs(curr);
        }
    }

}


function display(arr) {
    var ll = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.addElement(arr[i]);
    }
    ll.show();
    ll.reversing();
    console.log(reversList);
}
display([5,2,6,3,7,8,2,9,2]);
