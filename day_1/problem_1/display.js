/*
Name of the project : Dailychallenge
File name : display.js
Description : Find length of loop in the linked list
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [5,2,6,3,7,8,2,9,2]
Output : 9
*/


class Node {
    constructor(element) {
        this.element = element;
        this.next = null;
    }
}
var reversList = [];
class linkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    addElement(num) {
        if(this.head == null) {
            var node = new Node(num);
            this.head = node;
            this.size ++;
        }else {
            var prev ;
            var current = this.head;
            while(current) {
                prev = current;
                current = current.next;
            }
            var node = new Node(num);
            prev.next = node;
            this.size ++;
        }
    }
    show() {
      var curr = this.head;
      var str = "";
      while (curr) {
         str += curr.element + " ";
         curr = curr.next;
      }
      console.log(str);
    }
    check() {
        var flag = true;
        var elements = [];
        var curr = this.head;
        while(curr && flag) {
          //  if(elements.length != 0 ) {
                for (var i = 0; i < elements.length; i++) {
                     if((curr.next)!=null && curr.next == elements[i].next
                      && curr.element == elements[i].element) {
                        flag =false;
                        break;
                    }
            }
            if(flag) {
                elements.push(curr);
                curr=curr.next;
            }
            if(!flag) {
                 return elements.length-elements.indexOf("curr.next")-1;
            }
            if(!curr.next) {
               return 0;
            }
         }
     }
     swap(key1,key2) {
         var temp1,temp2;
         var flag1=true,flag2=true;
         var curr = this.head ;
         while(curr&&(flag1||flag2)) {
             if(curr.element == key1) {
                 flag1 = false ;
                 temp1 = curr;
             }
             if( curr.element==key2 ) {
                 falg2 = false ;
                 temp2 = curr ;
             }
             if((!false&&!false)||!curr)
                 break;
             else
                 curr = curr.next;
         }
         if(!flag1&&!flag2) {
             temp1.element = temp2.element;
         }
     }
     reversing() {
         var curr = this.head;
         if(curr) {
             rvrs(curr);
         }
     }
     rvrs(root) {
              if(root.next == null) {
                  result.push(root.element);
                  return;
              }else {
                  rvrs(root.next);
                  result.push(root.element);
                  return;
              }
     }
     show() {
       var curr = this.head;
       var str = "";
       while (curr) {
          str += curr.element + " ";
          curr = curr.next;
       }
       console.log(str);
     }
}


function display(arr) {
    var ll = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.addElement(arr[i]);
    }
    ll.show();
    var curr = ll.head,prv ,temp =curr.next;
    while(curr) {
       prv = curr;
       curr = curr.next;
    }
    prv.next = temp;
    console.log(prv.element);
    console.log(ll.check());
    var ll1 = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.addElement(arr[i]);
    }
    ll1.show();
    ll1.swap(3,8);
    ll1.show();
}

display([5,2,6,3,7,8,2,9,2]);
